﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    // Start is called before the first frame update
[SerializeField]
private Transform player;
    // Update is called once per frame
        [SerializeField]
    private Vector3 offset;
    void Update()
    {
        transform.position = player.position + offset;
    }
}
