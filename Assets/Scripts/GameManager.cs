﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private GameOverPanel gameOverPanel;
    [SerializeField]
    private WinPanel winPanel;

    public void WinGame(int score)
    {
        winPanel.SetPanelActive(true);
        winPanel.SetScoreText(score);
    }

    public void GameOver(int score)
    {
        gameOverPanel.SetPanelActive(true);
        gameOverPanel.SetScoreText(score);
    }

    // Update is called once per frame

}
