﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Для работы с ui

public class Player : MonoBehaviour
{
    [SerializeField]
    private GameManager gameManager;


    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private int coin;
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private float speed, sideSpeed;
    [SerializeField]
    private float accelSpeed; //на сколько ускоряется игрок

    [SerializeField]
    private float timerSet;
    [SerializeField]
    public float timer;

    private float tempSpeed; //Хранит предыдущее значение скорости

    void Start()
    {
        tempSpeed = speed;
        coin = 0;
        timer = 0;

        rb = GetComponent<Rigidbody>();
        scoreText.text = "0";
    }

    // Update is called once per frame
    void Update()
    {
        //tempSpeed=speed;
        rb.AddForce(0, 0, 15);
        rb.AddForce(0, 0, speed * Time.deltaTime);
        if (Input.GetKey("a"))
        {
            rb.AddForce(-sideSpeed * Time.deltaTime, 0, 0, ForceMode.Impulse);

        }

        if (Input.GetKey("d"))
        {
            rb.AddForce(sideSpeed * Time.deltaTime, 0, 0, ForceMode.Impulse);
        }
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else speed = tempSpeed;

        if (transform.position.y < 0)
        {
            gameManager.GameOver(coin);
        }


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            coin++;
            Destroy(other.gameObject); //Если кол-во монет кратно трем увеличивам скорость на 50
            if (coin % 3 == 0)
            {
                tempSpeed += 50;
            }

        }
        if (other.tag == "Accelerator")
        {
            tempSpeed = speed;
            this.speed = this.accelSpeed;
            this.timer = this.timerSet;
        }
        if (other.tag == "Finish")
        {
            this.enabled = false;
            gameManager.WinGame(coin);
        }
        scoreText.text = coin.ToString();


        Debug.Log("Trigger Hit" + other.tag);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Block")
        {

            this.enabled = false;
            gameManager.GameOver(coin);
        }

        Debug.Log("Collision Hit" + other.gameObject.name);
    }
}
