﻿
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private Transform root;
    [SerializeField]
    private Text yourScoreText, bestScoreText;

    [SerializeField]
    private Button resetBtn, endBtn;
    void Start()
    {
        root.gameObject.SetActive(false);
        resetBtn.onClick.AddListener(OnResetClick);
        endBtn.onClick.AddListener(OnEndClick);
    }

    public void SetPanelActive(bool state)
    {
        root.gameObject.SetActive(state);

    }

    public void SetScoreText(int yourScore)
    {

        int bestScore = PlayerPrefs.GetInt("BestScore");

        if (yourScore > bestScore)
        {
            bestScore = yourScore;
            PlayerPrefs.SetInt("BestScore", bestScore);
        }

        yourScoreText.text = "Your Score: " + yourScore;
        bestScoreText.text = "Best Score: " + bestScore;
    }

    private void OnResetClick()
    {
        Debug.Log("Reset Click");

    }

    private void OnEndClick()
    {

        Debug.Log("End Click");
    }

    private void OnDestroy()
    {
        resetBtn.onClick.RemoveAllListeners();
        endBtn.onClick.RemoveAllListeners();
    }

}
