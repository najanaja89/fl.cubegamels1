﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Button starBtn;
    [SerializeField]
    private Button exitBtn;

    void Start()
    {
        starBtn.onClick.AddListener(StartGame);
        exitBtn.onClick.AddListener(ExitGame);
    }

    private void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    private void ExitGame()
    {

        Application.Quit();
    }
    private void OnDestroy()
    {
        starBtn.onClick.RemoveAllListeners();
        exitBtn.onClick.RemoveAllListeners();

    }
}
