﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class WinPanel : MonoBehaviour
{
    [SerializeField]
    private Transform root;
    [SerializeField]
    private Text yourScoreText, bestScoreText;

    [SerializeField]
    private Button nextBtn, menuBtn;

    void Start()
    {
        root.gameObject.SetActive(false);
        nextBtn.onClick.AddListener(OnNextClick);
        menuBtn.onClick.AddListener(OnMenuClick);
    }

    public void SetPanelActive(bool state)
    {
        root.gameObject.SetActive(state);

    }

    public void SetScoreText(int yourScore)
    {

        int bestScore = PlayerPrefs.GetInt("BestScore");

        if (yourScore > bestScore)
        {
            bestScore = yourScore;
            PlayerPrefs.SetInt("BestScore", bestScore);
        }

        yourScoreText.text = "Your Score: " + yourScore;
        bestScoreText.text = "Best Score: " + bestScore;
    }

    private void OnNextClick()
    {
       SceneManager.LoadScene(2);

    }

    private void OnMenuClick()
    {
        SceneManager.LoadScene(0);
    }

    private void OnDestroy()
    {
        nextBtn.onClick.RemoveAllListeners();
        menuBtn.onClick.RemoveAllListeners();
    }
}
